import java.util.HashMap;

public class ConstructorsOfHashMap {
	
	@SuppressWarnings({"unchecked", "unused", "rawtypes"})
	public static void main(String[] args) throws IllegalArgumentException{
		//throws exception if the initial capacity is negative or the load factor is non positive
		
		HashMap<String, Integer> defaultCapacityDefaultLoad = new HashMap<String, Integer>(); // default capacity=16, load factor=0.75
		HashMap<String, Integer> setCapacityDefaultLoad = new HashMap<String, Integer>(32); // capacity=32, load factor=0.75
		HashMap<String, Integer> setCapacitySetLoad = new HashMap<String, Integer>(32, (float)0.60); // capacity=32, load factor=0.60
		HashMap specifiedMap = new HashMap(setCapacitySetLoad); // Constructs a new HashMap with the same mappings as the specified Map
		
		HashMap<String, Integer> test = new HashMap<String, Integer>(32, (float) 0.60);
		
		test.put(null, 1);
		test.put(null, 2);
		
		System.out.println(test.get(null));
		
		
		
		
		
	}
}
