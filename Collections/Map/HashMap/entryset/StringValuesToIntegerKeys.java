package map.hashmap.entryset;

import java.util.HashMap;

public class StringValuesToIntegerKeys {
	public static void main(String[] args) {
		HashMap<Integer, String> helloMap = new HashMap<Integer, String>();
		helloMap.put(1, "a");
		helloMap.put(2, "b");
		helloMap.put(3, "c");
		helloMap.put(4, "d");
		helloMap.put(5, "e");
		
		//Display HashMap
		System.out.println("Initial Mapping is: " +helloMap);
		
		//Display Set View
		System.out.println("Set view is: " +helloMap.entrySet());
	}
}
