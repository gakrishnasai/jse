package map.hashmap.entryset;

import java.util.HashMap;

public class IntegerValuesToStringKeys {
	public static void main(String[] args) {
		HashMap<String, Integer> helloMap = new HashMap<String, Integer>();
		helloMap.put("a",1);
		helloMap.put("b",2);
		helloMap.put("c",3);
		helloMap.put("d",4);
		helloMap.put("e",5);
		//Display Initial Mapping of HashMap
		System.out.println("HashMap is: " +helloMap);
		//Display Set View of HashMap
		System.out.println("Set view is: " +helloMap.entrySet());
	}
}
