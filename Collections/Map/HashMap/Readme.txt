Hierarchy
Extends the class -- AbstractMap<K,V>
Implements the interfaces -- Serializable, Cloneable, Map<K,V>

HashMap internally uses Array & LinkedList (array of buckets, buckets have linkelist node as below)

HashMap Node
int hash
K key
V value
Node next

K(key)/V(value) pairs
null keys	--	permits (only one, since the second one replaces the first one)
null values	--	permits (multiple)
unsynchronized
unordered
initial default capacity	--	16
load factor					--	0.75
capacity	--	number of buckets in the hash table
load factor	--	measure of how full the hash table is allowed to get before its capacity is automatically increased
rehash		--	twice the number of buckets

If iteration performance is important
Don't set initial capacity too high
or the load factor too low

Question: How HashMap works?
Answer:   On principle of Hashing

Question: What is Hashing?
process of converting an object into an integer value

when keys are Comparable, this class may use comparison order among keys to help break ties


COLLISSIONs

Sort HashMap in Java
	By Keys
		by using TreeMap
		by using LinkedHashMap
	By Values
		Comparator Interface